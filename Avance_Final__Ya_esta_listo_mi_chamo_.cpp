/*
 * nombrePrograma:
 * descripcion:
 *
 *
 *
 * autor: QUINTEROS PERALTA, Rodrigo Ervin
 * fecha:
 */

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
using namespace std;

int menuPrincipal();
int Titulo();
int menuLenguajeProgramacion();
int menuLenguajeProgramacionJAVA();
int menuLenguajeProgramacionC();
int menuLenguajeProgramacionCMasMas();
int menuLenguajeProgramacionPHYTON();
int menuBaseDatos();
int menuBaseDatos1();
int menuBaseDatos2();
int menuBaseDatos3();
int menuBaseDatos4();
int menuBaseDatos5();
int menuInteligenciaArtificial();
int menuInteligenciaArtificial1();
int menuInteligenciaArtificial2();
int menuInteligenciaArtificial3();
int menuInteligenciaArtificial4();
int menuInteligenciaArtificial5();
int menuMineriaDatos();
int menuMineriaDatos1();
int menuMineriaDatos2();
int menuMineriaDatos3();
int menuSistemasOperativos();
int menuSistemasOperativos1();
int menuSistemasOperativos2();
int menuSistemasOperativos3();
int menuSistemasOperativos4();
int menuSistemasOperativos5();
int menuSistemasOperativos6();
int menuArquitecturaSoftware();
int menuArquitecturaSoftware1();
int menuArquitecturaSoftware2();
int menuArquitecturaSoftware3();
int menuArquitecturaSoftware4();
int menuArquitecturaSoftware5();

int main()
{
   int opcion;
   char c[7];
   printf("INGRESE SU CODICO: ");
   scanf("%s",&c);
   if(c[0]=='1'&&c[1]=='8'){
      if(c[2]>='0'&&c[2]<='9'){
         if(c[3]>='0'&&c[3]<='9'){
            if(c[4]>='0'&&c[4]<='9'){
               if(c[5]>='0'&&c[5]<='9'){
                  if(c[6]>='0'&&c[6]<='9'){
                     if(c[7]>='0'&&c[7]<='9'){
                        do{
                           opcion=menuPrincipal();
                           switch(opcion){
                              case 0:
                                 exit(1);
                              case 1:
                                do{
                                    system("cls");
                                    opcion=menuLenguajeProgramacion();
                                    switch(opcion){
                                       case 0:
                                          break;
                                       case 1:
                                          menuLenguajeProgramacionJAVA();
                                          system("pause");
                                          break;
                                       case 2:
                                          menuLenguajeProgramacionC();
                                          system("pause");
                                          break;
                                       case 3:
                                          menuLenguajeProgramacionCMasMas();
                                          system("pause");
                                          break;
                                       case 4:
                                          menuLenguajeProgramacionPHYTON();
                                          system("pause");
                                          break;
                                    }
                                 }while(opcion!=0);
                                 opcion=-1;
                                 break;
                              case 2:
                                 do{
                                    system("cls");
                                    opcion=menuBaseDatos();
                                    switch(opcion){
                                       case 0:
                                          break;
                                       case 1:
                                          menuBaseDatos1();
                                          system("pause");
                                          break;
                                       case 2:
                                          menuBaseDatos2();
                                          system("pause");
                                          break;
                                       case 3:
                                          menuBaseDatos3();
                                          system("pause");
                                          break;
                                       case 4:
                                          menuBaseDatos4();
                                          system("pause");
                                          break;
                                       case 5:
                                          menuBaseDatos5();
                                          system("pause");
                                          break;
                                    }
                                 }while(opcion!=0);
                                 opcion=-1;
                                 break;
                              case 3:
                                 do{
                                    system("cls");
                                    opcion=menuInteligenciaArtificial();
                                    switch(opcion){
                                       case 0:
                                          break;
                                       case 1:
                                          menuInteligenciaArtificial1();
                                          system("pause");
                                          break;
                                       case 2:
                                          menuInteligenciaArtificial2();
                                          system("pause");
                                          break;
                                       case 3:
                                          menuInteligenciaArtificial3();
                                          system("pause");
                                          break;
                                       case 4:
                                          menuInteligenciaArtificial4();
                                          system("pause");
                                          break;
                                       case 5:
                                          menuInteligenciaArtificial5();
                                          system("pause");
                                          break;
                                    }
                                 }while(opcion!=0);
                                 opcion=-1;
                                 break;
                              case 4:
                                 do{
                                    system("cls");
                                    opcion=menuMineriaDatos();
                                    switch(opcion){
                                       case 0:
                                          break;
                                       case 1:
                                          menuMineriaDatos1();
                                          system("pause");
                                          break;
                                       case 2:
                                          menuMineriaDatos2();
                                          system("pause");
                                          break;
                                       case 3:
                                          menuMineriaDatos3();
                                          system("pause");
                                          break;
                                    }
                                 }while(opcion!=0);
                                 opcion=-1;
                                 break;
                              case 5:
                                 do{
                                    system("cls");
                                    opcion=menuSistemasOperativos();
                                    switch(opcion){
                                       case 0:
                                          break;
                                       case 1:
                                          menuSistemasOperativos1();
                                          system("pause");
                                          break;
                                       case 2:
                                          menuSistemasOperativos2();
                                          system("pause");
                                          break;
                                       case 3:
                                          menuSistemasOperativos3();
                                          system("pause");
                                          break;
                                       case 4:
                                          menuSistemasOperativos4();
                                          system("pause");
                                          break;
                                       case 5:
                                          menuSistemasOperativos5();
                                          system("pause");
                                          break;
                                       case 6:
                                          menuSistemasOperativos6();
                                          system("pause");
                                          break;
                                    }
                                 }while(opcion!=0);
                                 opcion=-1;
                                 break;
                              case 6:
                                 do{
                                    system("cls");
                                    opcion=menuArquitecturaSoftware();
                                    switch(opcion){
                                       case 0:
                                          break;
                                       case 1:
                                          menuArquitecturaSoftware1();
                                          system("pause");
                                          break;
                                       case 2:
                                          menuArquitecturaSoftware2();
                                          system("pause");
                                          break;
                                       case 3:
                                          menuArquitecturaSoftware3();
                                          system("pause");
                                          break;
                                       case 4:
                                          menuArquitecturaSoftware4();
                                          system("pause");
                                          break;
                                       case 5:
                                          menuArquitecturaSoftware5();
                                          system("pause");
                                          break;
                                    }
                                 }while(opcion!=0);
                                 opcion=-1;
                                 break;
                           }
                        }while(opcion!=0);
                     }
                  }
               }
            }
         }
      }
   }
   return (0);
   system ("pause");
}

int Titulo(){
   printf("\t\t\t\tBIBLIOTECA DE BASE 18\n\n");
}

int menuPrincipal(){
   system("cls");
   Titulo();
   int op;
   printf("\t\t\tEscoja el tema de su interes\n\n");
   printf("0. SALIR\n\n");
   printf("1. Lenguaje de programacion\n");
   printf("2. Base de datos\n");
   printf("3. Inteligencia Artificial\n");
   printf("4. Mineria de Datos\n");
   printf("5. Sistemas Operativos\n");
   printf("6. Arquitectura de Software\n");
   printf("============================\n");
   printf("      Escoja una opcion: \n");
   do{
      scanf("%d", &op);
   }while(op<0||op>6);
   return(op);
}

int menuLenguajeProgramacion(){
   int op;
   Titulo();
   printf("\t\t\t\tLENGUAJE DE PROGRAMACION\n\n");
   printf("\t\t\tEscoja el libro de su interes\n\n");
   printf("0. SALIR\n\n");
   printf("1. Java\n");
   printf("2. C\n");
   printf("3. C++\n");
   printf("4. PHYTON\n");
   do{
      printf("Digite su opcion: ");
      scanf("%d", &op);
   }while(op<0||op>4);
   return(op);
}

int menuLenguajeProgramacionJAVA(){
   printf("\n\nTITULO DE LIBRO: Java a fondo \n");
   printf("AUTOR: Pablo Augusto Sznajdleder");
   printf("\n\nTITULO DE LIBRO: INTRODUCCION A LA PROGRAMACION EN JAVA  \n");
   printf("AUTOR: DANIEL LIAN");
   printf("\n\nTITULO DE LIBRO: Introducci�n a la Programaci�n Orientada a Objetos con Java  \n");
   printf("AUTOR: C. THOMAS WU\n");
}
int menuLenguajeProgramacionC(){
   printf("\n\nTITULO DE LIBRO: EL LENGUAJE DE PROGRAMACION C \n");
   printf("AUTOR: BRIAN W. KERNIGHAN, DENNIS M. RITCHIE");
   printf("\n\nTITULO DE LIBRO: Empezar de cero a programar en lenguaje C \n");
   printf("AUTOR: Carlos Javier Pes Rivas");
   printf("\n\nTITULO DE LIBRO: Programaci�n en C\n");
   printf("AUTOR: Luis Joyanes y Zahonero Martinez\n");
}
int menuLenguajeProgramacionCMasMas(){
   printf("\n\nTITULO DE LIBRO: PROGRAMACION C++ CURSO DE INICIACION  \n");
   printf("AUTOR: EDGAR D. ANDREA");
   printf("\n\nTITULO DE LIBRO: C++ PARA INGENIERIA Y CIENCIAS  \n");
   printf("AUTOR: GARY J. BRONSON");
   printf("\n\nTITULO DE LIBRO: Thinking in C++\n");
   printf("AUTOR: Bruce Eckel\n");

}
int menuLenguajeProgramacionPHYTON(){
   printf("\n\nTITULO DE LIBRO: INTRODUCCION A LA PROGRAMACION CON PYTHON \n");
   printf("AUTOR: ANDRES MARZAL, ISABEL GRACIA");
   printf("\n\nTITULO DE LIBRO: Learn Python the Hard Way  \n");
   printf("AUTOR: Zed Shaw");
   printf("\n\nTITULO DE LIBRO: Python Para Informaticos: Explorando La Informacion \n");
   printf("AUTOR: Charles Severance\n");
}

int menuBaseDatos(){
   int op;
   Titulo();
   printf("\t\t\t\tBASE DE DATOS\n\n");
   printf("\t\t\tEscoja el libro de su interes\n\n");
   printf("0. SALIR\n\n");
   printf("1. Aprende SQL en un fin de semana \n");
   printf("2. BIG DATA con PYTHON. Recolecci�n, almacenamiento y proceso\n");
   printf("3. Big data como activo de negocio (Social Media)\n");
   printf("4. Dise�o de Bases de Datos \n");
   printf("5. Optimizacion SQL en Oracle\n");
   do{
      printf("Digite su opcion: ");
      scanf("%d", &op);
   }while(op<0||op>5);
   return(op);
}

int menuBaseDatos1(){
   printf("\n\nTITULO DE LIBRO: Aprende SQL en un fin de semana \n");
   printf("AUTOR: Antonio Padial Solier  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEl curso definitivo para crear y consultar bases de datos (Aprende en un fin de semana)\n");
   printf("\tSin necesidad de conocimientos previos. Aprende a manipular y consultar bases de datos de \n");
   printf("\tforma rapida y sencilla. Estas desarrollando una web y quieres utilizar MySQL para almacenar\n");
   printf("\tinformacion? estas estudiando y se te atraganta la asignatura de base de datos? quieres \n");
   printf("\taprender SQL para mejorar tu curriculum o dar un giro a tu vida laboral? quieres convertirte \n");
   printf("\ten un Cienifico de Datos y no sabes por donde empezar? o �simplemente tienes curiosidad por\n");
   printf("\tconocer este lenguaje y sus posibilidades? A todos vosotros bienvenidos, habeis dado con el  \n");
   printf("\tlibro adecuado.\n\n");
}

int menuBaseDatos2(){
   printf("\n\nTITULO DE LIBRO: BIG DATA con PYTHON. Recoleccion, almacenamiento y proceso\n");
   printf("AUTOR: Rafael Caballero Roldan\nAUTOR: Enrique Mart�n Mart�n Adri�n Riesco Rodr�guez  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEn este libro se presentan los conocimientos y las tecnolog�as que permitiran participar \n");
   printf("\ten esta nueva era de la informacion, regida por el Big Data y el aprendizaje autom�tico; \n");
   printf("\tse analiza la �vida� de los datos paso a paso, mostrando como obtenerlos, almacenarlos, \n");
   printf("\tprocesarlos, visualizarlos, y extraer conclusiones de ellos; es decir, mostrar el analisis  \n");
   printf("\tde datos tal y como es: un area fascinante, que requiere muchas horas de trabajo \n");
   printf("\tcuidadoso.\n\n");
}

int menuBaseDatos3(){
   printf("\n\nTITULO DE LIBRO: Big data como activo de negocio (Social Media) \n");
   printf("AUTOR: Gemma Mu�oz Vera\nAUTOR: Eduardo S�nchez Rojo \n\n");
   printf("DESCRIPCION: \n");
   printf("\tAun sin definirse plenamente y en constante evolucion, el Big Data se ha instalado en \n");
   printf("\tnuestras vidas. El reto actual es que esta cantidad ingente de datos impacte positivamente  \n");
   printf("\ten el negocio. Hasta ahora, el Big Data ha sido un pasivo para las compan�as,debido a la\n");
   printf("\tnecesaria inversion a la hora de capturar y almacenar los datos. Ha llegado el momento de que se \n");
   printf("\tconvierta en un activo fundamental y garantice el exito en la toma de decisiones de la organizaci�n.\n");
   printf("\tEste libro ayuda a transformar de forma transversal la informacion en conocimiento por medio de procesos. \n");
}

int menuBaseDatos4(){
   printf("\n\nTITULO DE LIBRO: Diseno de Bases de Datos  \n");
   printf("AUTOR: Sergio Garrido Barrientos  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEl dise�o de una base de datos es un punto critico y muy importante a la hora de crear \n");
   printf("\tuna aplicacion software.\n");
   printf("\tPartiendo de reglas claras, bien explicadas y siempre acompanadas de ejemplos aprenderas\n");
   printf("\ta disenar bases de datos de un modo facil y rapido. Aprende a pasar de una simple\n");
   printf("\tdescripcion o requisitos a un modelo conceptual, tambien llamado entidad-relacion y por \n");
   printf("\tultimo transformarlo al modelo relacional con el que ya trabajaremos en una aplicacion.\n\n");
}

int menuBaseDatos5(){
   printf("\n\nTITULO DE LIBRO: Optimizacion SQL en Oracle \n");
   printf("AUTOR: Javier Morales Carreras \n\n");
   printf("DESCRIPCION: \n");
   printf("\t-Como realizar el an�lisis y estudio de SQL ineficiente, incluyendo trucos y t�cnicas para  \n");
   printf("\tdesmenuzar las operaciones internas de sus ejecuciones.\n");
   printf("\t-Un estudio a fondo sobre el optimizador y sus comportamientos, as� como los principales \n");
   printf("\tparametros que afectan a la ejecucion de codigo SQL.\n");
   printf("\t-Uso de funciones analiticas, control de las ordenaciones implicitas, del impacto en el \n");
   printf("\trendimiento por el uso recursivo de funciones o un dise�o f�sico inadecuado.\n");
}

int menuInteligenciaArtificial(){
   int op;
   Titulo();
   printf("\t\t\t\tINTELIGENCIA ARTIFICIAL\n\n");
   printf("\t\t\tEscoja el libro de su interes\n\n");
   printf("0. SALIR\n\n");
   printf("1. Inteligencia artificial \n");
   printf("2. Mi Primer Perceptr�n con Python \n");
   printf("3. Inteligencia Artificial\n");
   printf("4. Lo inevitable\n");
   printf("5. Inteligencia artificial avanzada\n");
   do{
      printf("Digite su opcion: ");
      scanf("%d", &op);
   }while(op<0||op>5);
   return(op);
}

int menuInteligenciaArtificial1(){
   printf("\n\nTITULO DE LIBRO: Inteligencia artificial \n");
   printf("AUTOR: Pedro Meseguer Gonz�lez\nAUTOR: Ram�n L�pez de M�ntaras Badia \n\n");
   printf("DESCRIPCION: \n");
   printf("\t�Es posible construir m�quinas inteligentes? �Es el cerebro una m�quina? Estas dos   \n");
   printf("\tpreguntas han sido la obsesi�n de grandes pensadores durante siglos. Pero con el \n");
   printf("\tdesarrollo de la inteligencia artificial, ambas cuestiones se han acercado e incluso se han  \n");
   printf("\tunificado pues utilizan los mismos conceptos, t�cnicas y experimentos en los intentos \n");
   printf("\tde dise�ar m�quinas inteligentes y en investigar la naturaleza de la mente.  \n");
}

int menuInteligenciaArtificial2(){
   printf("\n\nTITULO DE LIBRO: Mi Primer Perceptr�n con Python \n");
   printf("AUTOR: Eric Joel Barrag�n Gonz�lez  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tAnalizado y Explicado con Sentido Pr�ctico (Programando Redes Neuronales Artificiales Paso a Paso con Python)\n");
   printf("\tCodifica tu propio Perceptr�n Simple linea a linea, explicado y analizado con sentido pr�ctico. \n");
   printf("\tComienza copiando y aprende con explicaciones y ejercicios guiados, permiti�ndote adquirir   \n");
   printf("\texperiencia para configurar tus propios Perceptr�nes, y acortar tu curva de aprendizaje, \n");
   printf("\tcompa��ndote con comentarios de mi propia experiencia. ''Mi Primer Perceptr�n'' \n");
}

int menuInteligenciaArtificial3(){
   printf("\n\nTITULO DE LIBRO: Inteligencia Artificial\n");
   printf("AUTOR: Jerry Kaplan  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEn el transcurso de las d�cadas venideras, la Inteligencia Artificial tendr� un profundo impacto \n");
   printf("\tsobre la forma en que vivimos, trabajamos, hacemos la guerra, jugamos, buscamos pareja, educamos  \n");
   printf("\ta nuestros j�venes y cuidamos de nuestros mayores. Es probable que aumente en gran medida   \n");
   printf("\tnuestra riqueza colectiva, pero tambi�n cambiar� dr�sticamente nuestro mercado de trabajo, \n");
   printf("\ttrastocar� nuestro orden social y forcejear� con nuestras instituciones privadas y p�blicas.  \n");
}

int menuInteligenciaArtificial4(){
   printf("\n\nTITULO DE LIBRO: Lo inevitable\n");
   printf("AUTOR: Kevin Kelly  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEntender las 12 fuerzas tecnol�gicas que configurar�n nuestro futuro  \n");
   printf("\tGran parte de lo que suceder� en los pr�ximos treinta a�os es inevitable, \n");
   printf("\timpulsado por las tendencias tecnol�gicas que ya est�n en movimiento.  \n");
}

int menuInteligenciaArtificial5(){
   printf("\n\nTITULO DE LIBRO: Inteligencia artificial avanzada\n");
   printf("AUTOR: Raul Ben�tez Igl�sias\nAUTOR: Gerard Escudero Bakx\nAUTOR: Samir Kanaan Izquierdo\nAUTOR: David Masip Rod�\n\n");
   printf("DESCRIPCION: \n");
   printf("\tEn este libro se introducen los conceptos fundamentales de la Inteligencia Artificial, desde una visi�n\n");
   printf("\tmuy orientada al reconocimiento de patrones. El lector podr� encontrar una revisi�n completa de las\n");
   printf("\tt�cnicas avanzadas m�s usadas en el campo del aprendizaje autom�tico. El texto se ha enfocado desde\n");
   printf("\tun punto de vista eminentemente pr�ctico. Las explicaciones te�ricas de las diferentes t�cnicas se\n");
   printf("\tbasan en la resoluci�n de problemas concretos.  \n");
}

int menuMineriaDatos(){
   int op;
   Titulo();
   printf("\t\t\t\tMINERIA DE DATOS\n\n");
   printf("0. SALIR\n\n");
   printf("1. Miner�a de datos: t�cnicas y herramientas  \n");
   printf("2. Mineria de datos \n");
   printf("3. An�lisis en un mundo de Big Data: la gu�a esencial para la ciencia de datos y sus aplicaciones  \n");
   do{
      printf("Digite su opcion: ");
      scanf("%d", &op);
   }while(op<0||op>3);
   return(op);
}

int menuMineriaDatos1(){
   printf("\n\nTITULO DE LIBRO: Mineria de datos: t�cnicas y herramientas \n");
   printf("AUTOR: Carlos P�rez\n\n");
   printf("DESCRIPCION: \n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
}

int menuMineriaDatos2(){
   printf("\n\nTITULO DE LIBRO: Mineria de datos\n");
   printf("AUTOR: Jose L. Balcazar\n\n");
   printf("DESCRIPCION: \n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
}

int menuMineriaDatos3(){
   printf("\n\nTITULO DE LIBRO: An�lisis en un mundo de Big Data: la gu�a esencial para la ciencia de datos y sus aplicaciones  \n");
   printf("AUTOR: Bart Baesens\n\n");
   printf("DESCRIPCION: \n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
   printf("\t\n");
}

int menuSistemasOperativos(){
   int op;
   Titulo();
   printf("\t\t\t\tSISTEMAS OPERATIVOS\n\n");
   printf("\t\t\tEscoja el libro de su interes\n\n");
   printf("0. SALIR\n\n");
   printf("1. El gran libro de android \n");
   printf("2. El gran libro de debian GNU/LINUX\n");
   printf("3. Administracion b�sica de sistemas operativos  \n");
   printf("4. UBUNTU LINUX: Instalacion y configuracion basica en equipos y servidores \n");
   printf("5. WINDOWS 10 (Actualizada a la �ltima version) \n");
   printf("6. Sistemas operativos modernos \n");
   do{
      printf("Digite su opcion: ");
      scanf("%d", &op);
   }while(op<0||op>6);
   return(op);
}

int menuSistemasOperativos1(){
   printf("\n\nTITULO DE LIBRO: EL GRAN LIBRO DE ANDROID \n");
   printf("AUTOR: JES�S TOM�S GIRONES  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tAndroid es la plataforma libre para el desarrollo de aplicaciones moviles creada por Google.  \n");
   printf("\tEn la actualidad se ha convertido en la plataforma lider frente a otras como iPhone o Windows Phone.\n");
   printf("\tLas aplicaciones Android est�n ampliando su rango de influencia. \n");
}

int menuSistemasOperativos2(){
   printf("\n\nTITULO DE LIBRO: EL GRAN LIBRO DE DEBIAN GNU/LINUX(\n");
   printf("AUTOR: Rafael Eduardo Rumbos");
   printf("DESCRIPCION: \n");
   printf("\tEste es un libro adecuado para aprender a crear b�sicas y complejas plataformas tecnol�gicas haciendo \n");
   printf("\tuso �nicamente de software libre. En su interior encontrar� una gran gama de procedimientos revisados \n");
   printf("\ty ajustados a las mejores pr�cticas de implementaci�n de infraestructura tecnol�gica. Esta extensa\n");
   printf("\tgu�a de f�cil lectura lo llevar� de la mano para lograr objetivos concretos con el sistema operativo \n");
   printf("\tDebian GNU/Linux. \n");
}

int menuSistemasOperativos3(){
   printf("\n\nTITULO DE LIBRO: ADMINISTRACI�N B�SICA DE SISTEMAS OPERATIVOS\n");
   printf("AUTOR: JULIO GOMEZ LOPEZ");
   printf("DESCRIPCION: \n");
   printf("\tA lo largo del libro se estudian los aspectos fundamentales relacionados con la gesti�n de servicios\n");
   printf("\t de los sistemas operativos Windows y GNU/Linux.  \n");
}

int menuSistemasOperativos4(){
   printf("\n\nTITULO DE LIBRO: UBUNTU LINUX: INSTALACION Y CONFIGURACION BASICA EN EQUIPOS Y SERVIDORES \n");
   printf("AUTOR: FRANCISCO JAVIER CARAZO GIL  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tUn t�rmino que hasta hace poco era exclusivo de los c�rculos m�s especializados en Ciencia y Tecnolog�a \n");
   printf("\tde la sociedad, se ha popularizado hasta llegar a conseguir un hueco cada d�a m�s notable en todos \n");
   printf("\tlos rincones de �sta. El mundo de los grandes servidores hace tiempo que cay� en manos de este sistema \n");
   printf("\toperativo proveniente de Finlandia y que, gracias a un sudafricano, ha conseguido comenzar a plantearse  \n");
   printf("\tcomo una soluci�n m�s que real para los usuarios de equipos de escritorio de todo el mundo. \n");
}

int menuSistemasOperativos5(){
   printf("\n\nTITULO DE LIBRO: WINDOWS 10 (ACTUALIZADA A LA ULTIMA VERSION) \n");
   printf("AUTOR: JOSE MARIA DELGADO  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tLas estad�sticas dicen que gran parte de los usuarios no utilizan m�s de un diez o un veinte por ciento \n");
   printf("\tde las posibilidades de Windows. Esto es debido, en muchos casos, al desconocimiento de las herramientas \n");
   printf("\tque ofrece. Este libro pretende ser una gu�a imprescindible para cualquier usuario habitual de Windows 10 \n");
   printf("\tque realmente tenga inquietud por conocer y obtener el m�ximo del sistema operativo. \n");
}

int menuSistemasOperativos6(){
   printf("\n\nTITULO DE LIBRO: SISTEMAS OPERATIVOS MODERNOS \n");
   printf("AUTOR: ANDREW S. TANENBAUM \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEn esta nueva edici�n se reordenaron los cap�tulos para colocar el material central al principio.  \n");
   printf("\tTambi�n se puso mayor �nfasis en el sistema operativo como el creador de las abstracciones.  \n");
   printf("\tLos procesos, espacios de direcciones virtuales y archivos son los conceptos clave que \n");
   printf("\tproporcionan los sistemas operativos \n");
}

int menuArquitecturaSoftware(){
   int op;
   Titulo();
   printf("\t\t\t\tARQUITECTURA DE SOFTWARE\n\n");
   printf("0. SALIR\n\n");
   printf("1. ARQUITECTURA DE SOFTWARE ORIENTADA A PATRONES: UN SISTEMA DE PATRONES\n");
   printf("2. PATRONES DE INTEGRACI�N EMPRESARIAL: DISE�AR, CONSTRUIR Y DESPLEGAR SOLUCIONES DE MENSAJER�A\n");
   printf("3. PARTEJEROS DE ARQUITECTURA EN LA PR�CTICA\n");
   printf("4. ARQUITECTURA LIMPIA  \n");
   printf("5. MANUAL DE ARQUITECTOS DE SOFTWARE \n");
   do{
      printf("Digite su opcion: ");
      scanf("%d", &op);
   }while(op<0||op>5);
   return(op);
}

int menuArquitecturaSoftware1(){

   printf("\n\nTITULO DE LIBRO: ARQUITECTURA DE SOFTWARE ORIENTADA A PATRONES: UN SISTEMA DE PATRONES \n");
   printf("AUTOR: FRANK BUSCHMANN\nAUTOR: REGINE MEUNIER\nAUTOR: HANS ROHNERT\nAUTOR: PETER SOMMERLAD\nAUTOR: MICHAEL STAL  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEste libro contiene los principios de la arquitectura de software, describiendo perfectamente varios   \n");
   printf("\testilos de arquitectura que, aunque algunos pueden considerarse en �desuso� (no obsoletos,   \n");
   printf("\tporque aprenderemos que aquello que alg�n dia es obsoleto puede volver a estar de �moda�) \n");
   printf("\tson principios muy �tiles y vigentes hoy en d�a y probablemente en un futuro. \n");
}
int menuArquitecturaSoftware2(){

   printf("\n\nTITULO DE LIBRO: PATRONES DE INTEGRACI�N EMPRESARIAL: DISE�AR, CONSTRUIR Y DESPLEGAR SOLUCIONES DE MENSAJER�A \n");
   printf("AUTOR: MARTIN FOWLER  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tcontienen un enfoque m�s contempor�neo a la arquitectura de software, refuerza los estilos \n");
   printf("\to patrones de los autores de POSA y tambien introduce nuevos, con una explicaci�n clara y usando  \n");
   printf("\treferencias a lenguajes de programaci�n �modernos�\n");
}
int menuArquitecturaSoftware3(){

   printf("\n\nTITULO DE LIBRO: PARTEJEROS DE ARQUITECTURA EN LA PR�CTICA\n");
   printf("AUTOR: LEN BASS\nAUTOR: PAUL CLEMENTS\nAUTOR: RICK KAZMAN \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEste libro es una de las mejores opciones a escoger en cuanto a recomendaciones y fundamentos de\n");
   printf("\tmuchos estilos o patrones de arquitecturas hoy en dia, quiz� mi favorito CQRS, un estilo moderno  \n");
   printf("\t(no siempre aplicable), incomprendido totalmente pero muy �til y vers�til.\n");
}
int menuArquitecturaSoftware4(){

   printf("\n\nTITULO DE LIBRO: ARQUITECTURA LIMPIA \n");
   printf("AUTOR: ROBERT MARTIN  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEste libro cuenta de forma muy pr�ctica su visi�n de la arquitectura a trav�s de su experiencia. \n");
   printf("\tDa ejemplos a nivel de arquitectura de cohesi�n, acoplamiento y los principios SOLID. Es \n");
   printf("\tinteresante su idea de que la base de datos, la web e incluso los frameworks de tu aplicaci�n  \n");
   printf("\tdeber�an ser solo detalles, es decir, piezas intercambiables de tu arquitectura.\n");
}
int menuArquitecturaSoftware5(){

   printf("\n\nTITULO DE LIBRO: MANUAL DE ARQUITECTOS DE SOFTWARE \n");
   printf("AUTOR: ROBERT MARTIN  \n\n");
   printf("DESCRIPCION: \n");
   printf("\tEste es un libro muy nuevo y poco conocido. Agrupa y contiene muchos conceptos importantes  \n");
   printf("\ten arquitectura. Es como un diccionario r�pido. Adem�s, involucra temas m�s actuales como\n");
   printf("\tarquitecturas serverless y microservicios.  \n");
}


#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#define MAX 10

using namespace std;

const int Max = 10;

struct LIBRO{
    int cod;
    char nom[40];
    float edicion;
};

int Menu();
void crearVector(LIBRO x[Max], int *dx);
void leerVector(LIBRO x[Max], int *dx);
void mostrarVector(LIBRO x[Max], int dx);
void encabezado1();
void raya58();
void raya();

int main()
{
    int na;
    int opcion, pos;
    LIBRO da, a[Max];
    //Si se desea utilizar varios vectores, como utilizaria crear vector()?
    crearVector(a, &na);
    do{
        opcion = Menu();
        switch(opcion)
        {
            case 0 :
                exit(1);
            case 1 :
                system("cls");
                cout <<"INGRESO DE DATOS DE LOS LIBROS \n";
                leerVector(a, &na);
                break;
            case 2 :
                system("cls");
                cout <<"RUTINA DE VIZUALIZACION\n";
                mostrarVector(a, na);
                break;
        }
    }while(opcion);
        printf("\n");
        system("PAUSE");
        return(0);
}

   
int Menu(){
    int op;
    system("cls");
    printf(" Bienvenidos a la FISITECA \n\n");
    printf("0. TERMINAR \n\n");
    printf("1. Ingreso de datos de los libros \n");
    printf("2. Mostrar los datos de los libros \n");
    printf("3. Insertar un libro en la posicion deseada \n");
    printf("4. Eliminar un libro en la posicion deseada \n");
    printf("5. Insertar por Valor \n");
    printf("6. Eliminar por Valor \n");
    printf("7. Ordenar Por Codigo Ascendente\n\n");
    do{
        printf("Digite la opcion que desea realizar ---> ");
        scanf("%d",&op);
    }while(op<0 || op>8);
    return(op);
}

void crearVector(LIBRO x[Max], int *dx)
{
    *dx=-1;
}

void leerVector(LIBRO x[Max], int *dx)
{
    int i, n, val;
    printf("\n\nNumero de Libros a ingresar ---> ");
    scanf("%d", &n);
    if(n < Max){
        for(i=0;i<n;i++){
            printf("\tLibro: %d\n", i+1);;
            cout <<" Codigo del libro ---> ";
            cin >>x[i].cod;
            cout <<" Nombre del libro ---> ";
            cin >>x[i].nom;
            cout <<" Edicion ---> ";
            cin >>x[i].edicion;
        }
        *dx = i;
    }
    else{
        printf(" %d dimension fuera de RANGO es mayor a Max = %d\n\n", n, Max);
        system("pause");
        exit(1);
    }
}
void encabezado1()
{
    system("cls");
    printf("\t\tREPORTE DE LIBROS REGISTRADOS\n\n");
//gotoxy(1,4);
    raya58();
 //gotoxy(1,5);
    printf(" Numero\tCodigo\tNombre\tEdicion\n");
 //gotoxy(1,6);
    raya();
}
void raya58()
{
printf("=========================================================\n");
}

void raya()
{
 printf("---------------------------------------------------------\n");
}

void mostrarVector(LIBRO x[Max], int dx)
{
    int i;
    if(dx>0){
        encabezado1();
        for(i=0;i<dx;i++){
            //gotoxy(5,7+i);
            printf("%d\t",i+1);
            //gotoxy(10,7+i);
            printf("%d\t",x[i].cod);
            //gotoxy(20,7+i);
            printf("%s\t",x[i].nom);
            //gotoxy(50,7+i);
            printf("%6.1f\n",x[i].edicion);
        }
        raya();
        cout <<"\n\n";
        system("pause");
    }
    else{
        printf("Vector vaciooo...\n\n");
    }
}

